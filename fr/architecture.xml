<?xml version="1.0" encoding="UTF-8"?>
<chapter id="architecture" lang="fr">
  <title>Architecture SOFI</title>

  <section>
    <title>Description</title>

    <para></para>
  </section>

  <section>
    <title>Gestion des caches avec implémentation de cache découplée (à
    venir)</title>

    <para>La gestion des cache permet de conserver temporairement des données
    qui sont réutilisées. Le principe de base d'une implémentation de cache
    est simple. Elle est composée d'une clé et d'une valeur. Des valeurs
    peuvent être ajoutées ou supprimées du cache à l'aide de la clé. Une
    classe d'implémentation de cache peut être injectée à la gestion des
    caches. Les implémentations de cache qui sont disponible dans SOFI sont
    CacheEhCacheImpl et CacheHashMapImpl</para>
  </section>

  <section>
    <title>Gestion des sous-caches</title>

    <para>La gestion des sous-caches permet d'emmagasiner par la gestion des
    caches des versions différentes d'un même nom de cache pour un identifiant
    de sous-cache. Un sous-cache peut être un identifiant de structure
    administrative ou une sécurité particulière. Par exemple, si une
    organisation désire que les utilisateurs d'un établissement ou d'un autre
    aient des valeurs de caches qui sont différentes. L'identifiant de
    l'établissement en cours doit être passé au système de cache.</para>

    <para>Les caches avec sous-caches sont toujours chargés "à la demande"
    c'est à dire que lorsque l'on demande un cache en particulier pour un
    nouvel identifiant de sous-cache, le gestionnaire de cache crée un nouvel
    ObjetCache puis appel sa méthode de chargement.</para>

    <section>
      <title>ObjetCache</title>

      <para>La classe d'objet cache est étandue pour recevoir un identifiant
      de sous-cache. C'est la responsabilité de l'implémentation du cache de
      charger une liste d'objets qui est contextuelle au sous-cache. La
      méthode chargerDonnees avec sous-cache doit être implémentée.</para>

      <para><example>
          <title>Méthode d'objet cache avec sous-cache</title>

          <para><screen>  public void chargerDonnees(Object sousCache) {
    // Faire son appel de service avec l'identifiant de sous-cache (exemple : etablissementId)
  }</screen></para>
        </example></para>

      <para>Ensuite, il doit utiliser l'identifiant de sous-cache avec les
      balises SOFI. La propriété sousCache supporte le JSTL et la valeur peut
      être de n'importe quel type d'objet.</para>
    </section>

    <section>
      <title>Balise select, cache et colonne</title>

      <para>Les trois balises touchés par cette fonctionnalité se voient
      toutes ajoutés la propriété "sousCache" qui permet</para>

      <para><example>
          <title>Balise select avec sous-cache</title>

          <para><screen>&lt;sofi-html:select 
  property="codeProvince" 
  cache="listeProvince"
  <emphasis role="bold">sousCache="${etablissementId}"</emphasis>
  proprieteEtiquette="description"
  proprieteValeur="valeur" /&gt;

&lt;sofi:cache 
  valeur="${codeProvince}"
  cache="listeProvince"
  <emphasis role="bold">sousCache="${etablissementId}"</emphasis>
  proprieteAffichage="description" /&gt;

&lt;sofi-liste:colonne libelle="Province" 
  property="codeProvince" 
  cache="listeProvince" 
  <emphasis role="bold">sousCache="${etablissementId}"</emphasis> /&gt;
</screen></para>
        </example></para>
    </section>
  </section>

  <section>
    <title>Filtre de cache</title>

    <para>Le filtre est utilisé pour altérer la collection des objets
    provenant du cache et qui seront utilisés par une balise. Il très
    important d'effectuer le clônage des éléments de la collection si l'on
    désire modifier des propriétés des objets existants.</para>

    <section>
      <title>Filtre pour la collection d'un objet cache</title>

      <para>Il est donc maintenant possible d'ajouter des valeurs
      dynamiquement au balises lorsque la balise récupère la liste provenant
      du cache. Par exemple, on peut imaginer qu'une organisation ait besoin
      de piloter leurs domaines de valeurs et d'activer ou inactiver certaines
      valeurs. Les unités de traitements qui utilisent ses liste de valeurs
      doivent être capable d'afficher quand même les valeurs qui sont
      maintenant inactives. Voici un exemple d'une implémentation de filtre
      qui ajoute dynamiquement la valeur inactive à la liste des valeurs d'un
      cache. Ainsi, lors de l'affichage d'une page avec des valeurs inactives
      les valeurs seront quand même affichées. Aussi, porter attention à la
      méthode getModele qui montre comment accéder au contexte Spring et donc
      au factory du modèle ADF dans Spring.</para>

      <para><example>
          <title>Filtre qui ajoute une valeur inactive</title>

          <para><screen>  <emphasis role="bold">public Object filtrer(Object collection, 
      BaliseValeurCache balise, PageContext pageContext) {</emphasis>
    LinkedHashMap listeValeur = new LinkedHashMap();

    if (collection != null) {
      Object value = balise.getValeurCache();
      
      if (value != null &amp;&amp; value instanceof String 
          &amp;&amp; !UtilitaireString.isVide((String) value)) {
        // Verifier si la valeur est dans la liste
        if (this.isValeurInactive((Map) collection, value)) {
          String domaineValeur = GestionCache.getInstance()
            .getContexte(balise.getNomCache()).getNomDomaineValeur();
          // Si la valeur n'est pas dans la liste
          ValeurElementPilote valeur = this.getModeleAbc(pageContext).getServicePilotage()
            .getValeurElementPiloteMemeExpirePourCodeEtEtablissement(
            domaineValeur, value.toString(), (Long) balise.getSousCache());
          Element element = new Element(valeur.getCode(), valeur.getDescriptionCourte());
          <emphasis role="bold">//La valeur inactive en premier</emphasis>
          <emphasis role="bold">listeValeur.put(element.getValeur(), element);</emphasis>
        }
      }
    }

<emphasis role="bold">    /* Le reste des valeurs, ici on ajoute directement
     * les valeurs. Mais si l'on avait à modifier les 
     * valeurs il faudrait cloner tous les éléments 
     * avant de les modifier.
     */ 
    listeValeur.putAll((Map) collection);</emphasis>
    
    return listeValeur;
  }
  
  /**
   * Est-ce que cette valeur est inactive?
   */
  private boolean isValeurInactive(Map collection, Object value) {
    boolean inactif = true;
    // La valeur est inactive si elle ne se retrouve pas dans le cache
    for (Iterator i = collection.keySet().iterator(); 
        i.hasNext() &amp;&amp; inactif; ) {
      String code = (String) i.next();
      if (code.equals(value)) {
        inactif = false;
      }
    }
    return inactif;
  }
  
  /**
   * Obtenir le modèle statique utilisé dans le contexte Spring.
   */
  public ModeleAbc getModeleAbc(PageContext pageContext) {
    return (ModeleAbc) WebApplicationModuleFactory
      .getModeleStatic("modeleFactory", pageContext.getRequest());
  }
</screen></para>
        </example></para>
    </section>

    <section>
      <title>Balise select, cache et colonne</title>

      <para>Les balises qui utilisent le gestionnaire de cache supportent
      toutes l'ajout d'un filtre.</para>

      <para><example>
          <title>Balise select avec filtre</title>

          <para><screen>&lt;sofi-html:select 
  property="codeProvince" 
  cache="listeProvince"
  sousCache="${etablissementId}"
  <emphasis role="bold">filtre="organisation.projet.presentation.filtre.FiltreValeurElementPiloteInactive"</emphasis>
  proprieteEtiquette="description"
  proprieteValeur="valeur" /&gt;

&lt;sofi:cache 
  valeur="${codeProvince}"
  cache="listeProvince"
  <emphasis role="bold">filtre="organisation.projet.presentation.filtre.FiltreValeurElementPiloteInactive"</emphasis>
  sousCache="${etablissementId}"
  proprieteAffichage="description" /&gt;

&lt;sofi-liste:colonne libelle="Province" 
  property="codeProvince" 
  cache="listeProvince" 
  <emphasis role="bold">filtre="organisation.projet.presentation.filtre.FiltreValeurElementPiloteInactive"</emphasis>  <emphasis
                role="bold">
  </emphasis>sousCache="${etablissementId}" /&gt;
</screen></para>
        </example></para>
    </section>
  </section>

  <section>
    <title>Support des valeurs par défaut par la balise Select.</title>

    <para>La balise Select utilise la méthode getCleDefaut de la classe
    ObjetCache. Par défaut la valeur par défaut est la première valeur de la
    liste. Cependant, il est possible de changer le comportement de sélection
    de la valeur pa défaut en surchargant la méthode getCleDefaut de votre
    classe d'objet cache. Dans l'exemple suivant, la valeur par défaut est
    identifiée par une propriété Boolean. On recherche la valeur qui possède
    la propriété</para>

    <para><example>
        <title>Implémentation de getCleDefaut d'un objet cache</title>

        <para><screen>  public Object getCleDefaut() {
    Object valeurDefaut = null;
    if (this.getListeValeur() != null) {
      for (Iterator i = this.getListeValeur().iterator(); i.hasNext() &amp;&amp; valeurDefaut == null; ) {
        Element valeur = (Element) i.next();
        if (valeur.getDefaut() != null &amp;&amp; valeur.getDefaut().booleanValue()) {
          valeurDefaut = valeur;
        }
      }
    }
    return valeurDefaut;
  }</screen></para>
      </example></para>

    <para>Pour activer la sélection d'une valeur par défaut simplement ajouter
    l'attribut "activerCleDefaut" dans la balise Select.</para>

    <para><example>
        <title>Balise Select avec sélection de valeur par défaut.</title>

        <para><screen>&lt;sofi-html:select 
  property="codeProvince" 
  cache="listeProvince"
  sousCache="${etablissementId}"
  proprieteEtiquette="description"
  proprieteValeur="valeur"
  filtre="organisation.projet.presentation.filtre.FiltreValeurElementPiloteInactive"
  <emphasis role="bold">activerCleDefaut="${bonneraison}"</emphasis>
  ligneVide="${ligneVide}" /&gt;</screen></para>
      </example></para>
  </section>
</chapter>