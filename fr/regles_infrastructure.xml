<?xml version="1.0" encoding="UTF-8"?>
<chapter id="regles_infrastructure" lang="fr">
  <title>Règles de nomenclature de l'infrastructure</title>

  <section>
    <title>Introduction</title>

    <para>Dans ce document vous retrouverez toute la documentation nécessaire
    aux règles de nomenclature qui sont importantes pour les architectes,
    analystes fonctionnelles et les développeurs dans l’utilisation de
    l’infrastructure SOFI. Parmi ces normes, vous allez voir comment vous
    devez décrire un libellé, un message ou un objet d’interface dans le
    référentiel SOFI. Afin de créer ces composants, il est fortement conseillé
    d’utiliser un outil de gestion Web offert par Nurun ou encore celui que
    votre organisation a développé.</para>
  </section>

  <section>
    <title>Norme pour les libellés</title>

    <para>Les libellés, l’aide contextuelle et l’aide en ligne qui sont
    spécifiés dans le référentiel SOFI, doivent respecter une norme afin de
    facilité son utilisation, son repérage, ainsi que les migrations des
    composants.</para>

    <para><example>
        <title>Norme</title>

        <para><screen>[code_application].[type].[service].[composant de page]</screen></para>

        <para>Tous les libellés, aide contextuelle ou aide en ligne doit
        débuter par 4 éléments standards.</para>
      </example></para>

    <table>
      <title>Description des éléments de la clé</title>

      <tgroup cols="2">
        <colspec align="left" colwidth="1*" />

        <colspec align="left" colwidth="4*" />

        <thead>
          <row>
            <entry>Élément</entry>

            <entry>Description</entry>
          </row>
        </thead>

        <tbody>
          <row>
            <entry>code_application</entry>

            <entry>Le code d’application ou de système. Exemple :infra_sofi,
            cours_sofi</entry>
          </row>

          <row>
            <entry>type</entry>

            <entry>Le type de composant qui décrit la clé. Valeurs possibles
            :libelle, aide</entry>
          </row>

          <row>
            <entry>service</entry>

            <entry>Le nom du service dont le composant qui est associé. Si le
            composant est commun à toute l’application veuillez spécifier
            commun. Exemple : gestion_libelle, commun</entry>
          </row>

          <row>
            <entry>type de composant</entry>

            <entry><para>Le type de composant dont représente la valeur de la
            clé.</para><para>Valeurs possibles :</para><itemizedlist>
                <listitem>
                  <para>section</para>
                </listitem>

                <listitem>
                  <para>service</para>
                </listitem>

                <listitem>
                  <para>onglet</para>
                </listitem>

                <listitem>
                  <para>bloc (Une section dans un page, tel qu’un « fieldset
                  »)</para>
                </listitem>

                <listitem>
                  <para>champ (Toutes les champs d’édition)</para>

                  <para>Il est proposé d’ajouter le nom de l’attribut java
                  après le type champ. Voici un exemple :
                  login_sofi.libelle.authentification.champ.codeUtilisateur</para>

                  <para>À noter qu’un nom d’attribut Java est un nom
                  conceptuel qui débute par une minuscule et que chacun des
                  mots suivants débutent par une majuscule.</para>
                </listitem>

                <listitem>
                  <para>action (Pour les boutons ou lien hypertexte qui
                  appelle une action précise tel qu’un enregistrement, une
                  suppression, une modification, etc.</para>
                </listitem>
              </itemizedlist></entry>
          </row>
        </tbody>
      </tgroup>
    </table>

    <para>Voici une liste d'exemple d'utilisation des clés de libellé avec des
    valeurs exemples :</para>

    <informaltable>
      <tgroup cols="2">
        <colspec align="left" colwidth="150" />

        <tbody>
          <row>
            <entry><emphasis role="bold">Clé</emphasis></entry>

            <entry>infra_sofi.libelle.gestion_parametre.service</entry>
          </row>

          <row>
            <entry><emphasis role="bold">Valeur en français
            (FR)</emphasis></entry>

            <entry>Gestion des paramètres systèmes</entry>
          </row>

          <row>
            <entry><emphasis role="bold">Valeur en anglais
            (EN)</emphasis></entry>

            <entry></entry>
          </row>
        </tbody>
      </tgroup>
    </informaltable>

    <informaltable>
      <tgroup cols="2">
        <colspec align="left" colwidth="150" />

        <tbody>
          <row>
            <entry><emphasis role="bold">Clé</emphasis></entry>

            <entry>infra_sofi.libelle.gestion_parametre.onglet.recherche</entry>
          </row>

          <row>
            <entry><emphasis role="bold">Valeur en français
            (FR)</emphasis></entry>

            <entry>Recherche</entry>
          </row>

          <row>
            <entry><emphasis role="bold">Valeur en anglais
            (EN)</emphasis></entry>

            <entry></entry>
          </row>
        </tbody>
      </tgroup>
    </informaltable>

    <informaltable>
      <tgroup cols="2">
        <colspec align="left" colwidth="150" />

        <tbody>
          <row>
            <entry><emphasis role="bold">Clé</emphasis></entry>

            <entry>infra_sofi.libelle.gestion_parametre.onglet.detail</entry>
          </row>

          <row>
            <entry><emphasis role="bold">Valeur en français
            (FR)</emphasis></entry>

            <entry>Paramètre système</entry>
          </row>

          <row>
            <entry><emphasis role="bold">Valeur en anglais
            (EN)</emphasis></entry>

            <entry></entry>
          </row>
        </tbody>
      </tgroup>
    </informaltable>

    <informaltable>
      <tgroup cols="2">
        <colspec align="left" colwidth="150" />

        <tbody>
          <row>
            <entry><emphasis role="bold">Clé</emphasis></entry>

            <entry>infra_sofi.libelle.gestion_parametre.champ.nom</entry>
          </row>

          <row>
            <entry><emphasis role="bold">Valeur en français
            (FR)</emphasis></entry>

            <entry>Nom :</entry>
          </row>

          <row>
            <entry><emphasis role="bold">Valeur en anglais
            (EN)</emphasis></entry>

            <entry></entry>
          </row>
        </tbody>
      </tgroup>
    </informaltable>

    <informaltable>
      <tgroup cols="2">
        <colspec align="left" colwidth="150" />

        <tbody>
          <row>
            <entry><emphasis role="bold">Clé</emphasis></entry>

            <entry>infra_sofi.libelle.gestion_parametre.action.rechercher</entry>
          </row>

          <row>
            <entry><emphasis role="bold">Valeur en français
            (FR)</emphasis></entry>

            <entry>Rechercher</entry>
          </row>

          <row>
            <entry><emphasis role="bold">Valeur en anglais
            (EN)</emphasis></entry>

            <entry></entry>
          </row>
        </tbody>
      </tgroup>
    </informaltable>

    <informaltable>
      <tgroup cols="2">
        <colspec align="left" colwidth="150" />

        <tbody>
          <row>
            <entry><emphasis role="bold">Clé</emphasis></entry>

            <entry>infra_sofi.libelle.gestion_parametre.action.enregistrer</entry>
          </row>

          <row>
            <entry><emphasis role="bold">Valeur en français
            (FR)</emphasis></entry>

            <entry>Enregistrer</entry>
          </row>

          <row>
            <entry><emphasis role="bold">Valeur en anglais
            (EN)</emphasis></entry>

            <entry></entry>
          </row>
        </tbody>
      </tgroup>
    </informaltable>

    <informaltable>
      <tgroup cols="2">
        <colspec align="left" colwidth="150" />

        <tbody>
          <row>
            <entry><emphasis role="bold">Clé</emphasis></entry>

            <entry>infra_sofi.libelle.commun.bloc.critere_recherche</entry>
          </row>

          <row>
            <entry><emphasis role="bold">Valeur en français
            (FR)</emphasis></entry>

            <entry>Critère(s) de recherche</entry>
          </row>

          <row>
            <entry><emphasis role="bold">Valeur en anglais (EN)</emphasis>
            <emphasis role="bold"></emphasis></entry>

            <entry></entry>
          </row>
        </tbody>
      </tgroup>
    </informaltable>

    <informaltable>
      <tgroup cols="2">
        <colspec align="left" colwidth="150" />

        <tbody>
          <row>
            <entry><emphasis role="bold">Clé</emphasis></entry>

            <entry>infra_sofi.libelle.commun.champ.nas</entry>
          </row>

          <row>
            <entry><emphasis role="bold">Valeur en français
            (FR)</emphasis></entry>

            <entry>Nas</entry>
          </row>

          <row>
            <entry><emphasis role="bold">Valeur en anglais (EN)</emphasis>
            </entry>

            <entry></entry>
          </row>
        </tbody>
      </tgroup>
    </informaltable>
  </section>

  <section>
    <title>Norme pour les message</title>

    <para>Pour tous les types de messages, que ce soit le type en erreur,
    celui de confirmation, d’avertissement ou d’information, qui sont
    spécifiés dans le référentiel SOFI. Vous devez respecter cette norme afin
    de faciliter l’utilisation, le repérage, et les migrations des
    messages.</para>

    <para><example>
        <title>Norme</title>

        <para><screen>[code_application].[type].[service].[champ en concerné]</screen></para>

        <para><table>
            <title>Description des éléments de la clé</title>

            <tgroup cols="2">
              <colspec align="left" colwidth="1*" />

              <colspec align="left" colwidth="4*" />

              <thead>
                <row>
                  <entry>Élément</entry>

                  <entry>Description</entry>
                </row>
              </thead>

              <tbody>
                <row>
                  <entry>code_application</entry>

                  <entry>Le code d’application ou de système. Exemple
                  :infra_sofi, cours_sofi</entry>
                </row>

                <row>
                  <entry>type</entry>

                  <entry>Le type de composant qui décrit la clé. Valeurs
                  possibles : erreur, information, confirmation,
                  avertissement</entry>
                </row>

                <row>
                  <entry>service</entry>

                  <entry><para>Le nom du service dont le composant qui est
                  associé. Si le composant est commun à toute l’application
                  veuillez spécifier commun. Si le nom du service est conçu de
                  plusieurs mots, vous devez séparer ces mots par un caractère
                  de soulignement (_).</para><para>Exemple : gestion_libelle,
                  commun</para></entry>
                </row>

                <row>
                  <entry>champ concerné</entry>

                  <entry><para>Le nom de l’attribut java qui est concerné par
                  le message. Vous devez spécifier le nom qui se retrouve dans
                  votre formulaire et dans la page JSP.</para><para>Exemple :
                  nom, adresseCivique</para></entry>
                </row>
              </tbody>
            </tgroup>
          </table></para>

        <para>Voici une liste d'exemple d'utilisation des clés de message avec
        des valeurs exemples : </para>

        <informaltable>
          <tgroup cols="2">
            <colspec align="left" colwidth="150" />

            <tbody>
              <row>
                <entry><emphasis role="bold">Clé</emphasis></entry>

                <entry>infra_sofi.erreur.gestion_parametre.codeApplication</entry>
              </row>

              <row>
                <entry><emphasis role="bold">Valeur en français
                (FR)</emphasis></entry>

                <entry>La clé de votre message d’erreur doit avoir le code
                d’application ou système.</entry>
              </row>

              <row>
                <entry><emphasis role="bold">Valeur en anglais
                (EN)</emphasis></entry>

                <entry></entry>
              </row>
            </tbody>
          </tgroup>
        </informaltable>

        <informaltable>
          <tgroup cols="2">
            <colspec align="left" colwidth="150" />

            <tbody>
              <row>
                <entry><emphasis role="bold">Clé</emphasis></entry>

                <entry>infra_sofi.information.gestion_parametre.suppresion.succes</entry>
              </row>

              <row>
                <entry><emphasis role="bold">Valeur en français
                (FR)</emphasis></entry>

                <entry>La suppression du paramètre a été effectuée avec
                succès.</entry>
              </row>

              <row>
                <entry><emphasis role="bold">Valeur en anglais
                (EN)</emphasis></entry>

                <entry></entry>
              </row>
            </tbody>
          </tgroup>
        </informaltable>

        <informaltable>
          <tgroup cols="2">
            <colspec align="left" colwidth="150" />

            <tbody>
              <row>
                <entry><emphasis role="bold">Clé</emphasis></entry>

                <entry>infra_sofi.confirmation.gestion_parametre.suppresion</entry>
              </row>

              <row>
                <entry><emphasis role="bold">Valeur en français
                (FR)</emphasis></entry>

                <entry>Êtes-vous sûr de vouloir supprimer ce
                paramètre?</entry>
              </row>

              <row>
                <entry><emphasis role="bold">Valeur en anglais
                (EN)</emphasis></entry>

                <entry></entry>
              </row>
            </tbody>
          </tgroup>
        </informaltable>

        <informaltable>
          <tgroup cols="2">
            <colspec align="left" colwidth="150" />

            <tbody>
              <row>
                <entry><emphasis role="bold">Clé</emphasis></entry>

                <entry>infra_sofi.avertissement.commun.champ.obligatoire</entry>
              </row>

              <row>
                <entry><emphasis role="bold">Valeur en français
                (FR)</emphasis></entry>

                <entry>Vous devez saisir tous les champs qui sont précédés de
                *.</entry>
              </row>

              <row>
                <entry><emphasis role="bold">Valeur en anglais
                (EN)</emphasis></entry>

                <entry></entry>
              </row>
            </tbody>
          </tgroup>
        </informaltable>

        <para></para>
      </example></para>
  </section>
</chapter>