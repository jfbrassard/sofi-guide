<?xml version="1.0" encoding="UTF-8"?>
<chapter id="service_web" lang="fr">
  <title>Services Web avec Apache CXF</title>

  <section>
    <title>Description</title>

    <para>Le framework de services web CXF permet d'exposer des méthode de
    logique d'affaire. Il est formtement intégré avec le framework
    Spring.</para>

    <para>Deux grandes stratégies sont possible pour réaliser un service web.
    Il est possible de démarrer le développement avec la conception de classes
    Java puis de générer le WSDL lors de l'exécution du programme. Cette
    startégie est aussi plus facilement applicable lorsque l'on désire exposer
    de la logique d'affaire dans un système existant. La deuxième approche est
    de produire un WSDL en premier puis de générer des classes Java.</para>

    <para>L'approche la plus simple est cependant de générer le WSDL car elle
    nécessite seulement une connaissance du Java et non une maîtrise de la
    structure des WSDL. Comme les compétences Java sont de loins plus
    répendues que les compétences en WSDL, ce sera l'approche de base
    prévilériée pour les développements SOFI. L'approche Java élimine aussi le
    code Java généré par le framework.</para>

    <para>Il faut cependant prendre consience que les classes et l'interface
    du service qui est exposé décide directement de la structure du WSDL. Les
    modifications aux objets et aux interfaces exposées devront être
    planifiées et négociées avec les partenaires externes qui utilisent les
    services web. On peut aussi décider de faire des services et objets qui
    sont spécialement fait pour être des services web. Cette stratégie permet
    de garder un meilleur contrôle sur l'évolution du service web car des
    classes et interfaces dédiés au services web sont exposés aux
    clients.</para>
  </section>

  <section>
    <title>Configuration de CXF</title>

    <para>Le framework CXF est constitué d'un servlet qui répond aux requêtes
    HTTP. Le servlet HTTP délaigue les appels aux beans de CXF instancés dans
    le conteneur Spring de l'application. Le client peut être basé sur
    n'importe quelle technologie qui supporte les services web standard. Les
    applications Java Spring peuvent utiliser le framework CXF pour générer un
    proxy qui imite le service Java du côté serveur. Le proxy en question
    converti les appels Java en message SOAP puis converti la réponse du
    service web en java. Cette fonctionnalité rend l'utilisation du service
    web transparente à l'application client.</para>

    <para>Voici un schéma général de l'appel d'un client et de la réception du
    message par le serveur Java à l'aide de CXF.</para>

    <screenshot>
      <screeninfo>Appel d'un client à un service web CXF</screeninfo>

      <mediaobject>
        <imageobject>
          <imagedata fileref="images/serviceweb/appel_service_cxf.jpg" />
        </imageobject>
      </mediaobject>
    </screenshot>

    <section>
      <title>Servlet CXF</title>

      <para>Le servlet est déclaré dans le fichier web.xml. Dans l'exemple
      ci-dessous, le servlet répond aux requêtes faites au contexte
      "/ws/*".</para>

      <para><example>
          <title>fichier web.xml</title>

          <para><screen>&lt;!-- Fichier qui contient la configuration de CXF et des endpoint --&gt;
&lt;context-param&gt;
  &lt;param-name&gt;contextConfigLocation&lt;/param-name&gt;
  &lt;param-value&gt;
    classpath:/com/nurun/preuveconcept/presentation/conf/service_web.xml
  &lt;/param-value&gt;
&lt;/context-param&gt;

&lt;!-- Initialise le contexte Spring --&gt;
&lt;listener&gt;
  &lt;listener-class&gt;
  org.springframework.web.context.ContextLoaderListener
  &lt;/listener-class&gt;
&lt;/listener&gt;

&lt;!-- Servlet qui traite les appels 
     aux services web CXF --&gt;
&lt;servlet&gt;
  &lt;servlet-name&gt;CXFServlet&lt;/servlet-name&gt;
  &lt;servlet-class&gt;
  org.apache.cxf.transport.servlet.CXFServlet
  &lt;/servlet-class&gt;
  &lt;load-on-startup&gt;1&lt;/load-on-startup&gt;
&lt;/servlet&gt;

&lt;!-- Appels vont être 
     http://host:port/contextroot/ws/monService --&gt;
&lt;servlet-mapping&gt;
  &lt;servlet-name&gt;CXFServlet&lt;/servlet-name&gt;
  &lt;url-pattern&gt;/ws/*&lt;/url-pattern&gt;
&lt;/servlet-mapping&gt;
</screen></para>
        </example></para>
    </section>

    <section>
      <title>Configuration Spring de CXF et des services webs</title>

      <para>Le servlet CXF fait appel a des beans qui sont déclarés dans le
      contexte Spring. Les beans sont rasemblés dans un fichier nommé
      "service_web.xml" dans le projet web de votre application.</para>

      <para>Ajouter un fichier "service_web.xml" à votre projet dans le
      paquetage "[organisation].[projet].presentation.conf".</para>

      <para><example>
          <title>fichier service_web.xml</title>

          <para><screen>&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:cxf="http://cxf.apache.org/core"
       xmlns:simple="http://cxf.apache.org/simple"
       xmlns:jaxws="http://cxf.apache.org/jaxws"
       xsi:schemaLocation="
http://www.springframework.org/schema/beans 
http://www.springframework.org/schema/beans/spring-beans-2.0.xsd
http://cxf.apache.org/core 
http://cxf.apache.org/schemas/core.xsd
http://cxf.apache.org/simple 
http://cxf.apache.org/schemas/simple.xsd
http://cxf.apache.org/jaxws 
http://cxf.apache.org/schemas/jaxws.xsd"&gt;
  
&lt;!-- Beans CXF --&gt;
 
&lt;import resource="<emphasis role="bold">classpath:META-INF/cxf/cxf.xml</emphasis>" /&gt;
&lt;import resource="<emphasis role="bold">classpath:META-INF/cxf/cxf-extension-soap.xml</emphasis>" /&gt;
&lt;import resource="<emphasis role="bold">classpath:META-INF/cxf/cxf-servlet.xml</emphasis>" /&gt;

&lt;!-- Journalisation des messages (seulement pour le débugage) --&gt;

&lt;bean id="logInbound" 
      class="org.apache.cxf.interceptor.LoggingInInterceptor"/&gt;
&lt;bean id="logOutbound" 
      class="org.apache.cxf.interceptor.LoggingOutInterceptor"/&gt;
&lt;cxf:bus&gt;
    &lt;cxf:inInterceptors&gt;
        &lt;ref bean="logInbound"/&gt;
    &lt;/cxf:inInterceptors&gt;
    &lt;cxf:outInterceptors&gt;
        &lt;ref bean="logOutbound"/&gt;
    &lt;/cxf:outInterceptors&gt;
    &lt;cxf:inFaultInterceptors&gt;
        &lt;ref bean="logOutbound"/&gt;
    &lt;/cxf:inFaultInterceptors&gt;
&lt;/cxf:bus&gt; 
&lt;cxf:bus&gt;
    &lt;cxf:features&gt;
        &lt;cxf:logging/&gt;
    &lt;/cxf:features&gt;
&lt;/cxf:bus&gt; 

&lt;/beans&gt;</screen></para>
        </example></para>
    </section>
  </section>

  <section>
    <title>Développement d'un service</title>

    <para>Le service est composé d'au minimum deux parties. Une interface qui
    décrit les opérations qui seront exposées en service web. La deuxième
    partie est l'implémentation du service.</para>

    <section>
      <title>Interface du service</title>

      <para>L'interface du service est une simple interface Java placé dans un
      projet de la couche modèle. Il est aussi possible d'ajouter des
      annotations pour personnaliser le WSDL qui sera généré par CXF. Il est
      recommandé de créer une interface spécialement pour l'exposition en
      service web. Si le service est aussi utilisé en service local on devrait
      toujours créer aussi une interface locale. Placer la nouvelle interface
      dans un paquetage "[organisation].[projet].[facette].service.ws". On
      doit ajouter l'annotation "@webservice" pour signifier à CXF que c'est
      cette interface qui sera utilisée pour la génération du WSDL.</para>

      <para><example>
          <title>Exemple d'interface de service web</title>

          <para><screen>package com.nurun.preuveconcept.modele.dossier.service.ws;

import java.util.List;

<emphasis role="bold">import javax.jws.WebService;</emphasis>

import com.nurun.preuveconcept.modele.dossier.entite.Dossier;
import com.nurun.preuveconcept.modele.dossier.entite.Evenement;
import com.nurun.sofi.composantweb.liste.ListeNavigation;

<emphasis role="bold">@WebService</emphasis>
public interface ServiceDossier {

  void test();

  Dossier getDossier(Long idDossier);

  List&lt;Evenement&gt; getListeEvenementPourDossier(Long dossierId);

  ListeNavigation getListeDossier(ListeNavigation liste);
}</screen></para>
        </example></para>
    </section>

    <section>
      <title>Implémentation du service</title>

      <para>Le service doit implémenter l'interface ci-dessus. Voici un
      exemple de l'implémentation du service d'affaire. L'annotation
      "@webservice" doit être placé au niveau de la déclaration de classe.
      L'attribut "endpointInterface" de l'annotation identifie quelle
      interface implémentée par la classe doit être utilisée pour générer le
      WSDL du service web.</para>

      <para><example>
          <title>Implémentation du service</title>

          <para><screen>package com.nurun.preuveconcept.modele.dossier.service.local;

import java.util.List;

<emphasis role="bold">import javax.jws.WebService;</emphasis>

import com.nurun.preuveconcept.modele.dossier.dao.EvenementDao;
import com.nurun.preuveconcept.modele.dossier.entite.Dossier;
import com.nurun.preuveconcept.modele.dossier.entite.Evenement;
import com.nurun.preuveconcept.modele.dossier.service.ServiceDossier;
import com.nurun.sofi.composantweb.liste.ListeNavigation;
import com.nurun.sofi.modele.spring.service.local.DaoServiceImpl;

<emphasis role="bold">@WebService(endpointInterface = "com.nurun.preuveconcept.modele.dossier.service.ws.ServiceDossier")</emphasis>
public class ServiceDossierImpl extends DaoServiceImpl 
  implements ServiceDossier, <emphasis role="bold">com.nurun.preuveconcept.modele.dossier.service.ws.ServiceDossier</emphasis> {

private EvenementDao evenementDao = null;

/*
 * (non-Javadoc)
 * @see com.nurun.preuveconcept.modele.dossier.service.ws.ServiceDossier#test()
 */
public void test() {
  System.out.println("test!");
}
...
</screen></para>
        </example></para>
    </section>

    <section>
      <title>Configuration service dans Spring</title>

      <para>Le service d'affaire doit être déclaré dans le contexte Spring.
      Voici la déclaration du service dans le fichier de service.xml du projet
      de la couche modèle sous le paquetage
      "[organisation].[projet].modele.conf".</para>

      <para><example>
          <title>Fichier service.xml</title>

          <para><screen>&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xmlns:tx="http://www.springframework.org/schema/tx"	
       xsi:schemaLocation="
http://www.springframework.org/schema/beans 
http://www.springframework.org/schema/beans/spring-beans-2.0.xsd
http://www.springframework.org/schema/tx 
http://www.springframework.org/schema/tx/spring-tx-2.0.xsd
http://www.springframework.org/schema/aop 
http://www.springframework.org/schema/aop/spring-aop-2.0.xsd"&gt;

&lt;!-- Services --&gt;
  
&lt;bean id="<emphasis role="bold">serviceDossier</emphasis>" parent="serviceLocal"
      class="com.nurun.preuveconcept.modele.dossier.service.local.ServiceDossierImpl"&gt;
  &lt;property name="dao" ref="dossierDao" /&gt;
  &lt;property name="evenementDao" ref="evenementDao" /&gt;
&lt;/bean&gt;

&lt;/beans&gt;</screen></para>
        </example></para>

      <para>Ensuite, un "endpoint" doit être déclaré dans le fichier
      "service_web.xml" créé précédement. Cette confguration permet de
      spécifier quel service sera disponible en service web et à quelle
      adresse.</para>

      <para>L'attribut "implementor" permet de spécifier quel bean est le
      service à exposer. Si la classe est la cible d'un ou plusieurs
      intercepteurs AOP, il est nécessaire de spécifier l'interface qui sera
      exposée en service web avec la propriété "implementorClass". En fait,
      sans cette propriété CXF inspecte un proxy et génère un WSDL qui ne
      correspond pas aux opérations désirées. Pour finir, la propriété
      "address" permet de spécifier à quel URL le service web sera disponible.
      La génération du WSDL est exécutée à l'aide de l'implémentation d'apache
      de la spécification "Java API for XML Web Services (JAX-WS)
      specification" JSR 224.</para>

      <para><example>
          <title>Fichier service_web.xml</title>

          <para><screen>&lt;!-- Exposer un service --&gt;

&lt;jaxws:endpoint 
  id="serviceDossierWs" 
  implementor="<emphasis role="bold">#serviceDossier</emphasis>" 
  implementorClass="<emphasis role="bold">com.nurun.preuveconcept.modele.dossier.service.ws.ServiceDossier</emphasis>" 
  address="<emphasis role="bold">/serviceDossier</emphasis>" /&gt;</screen></para>
        </example></para>
    </section>
  </section>

  <section>
    <title>Création d'un service client</title>

    <para>Le client peut être généré par un outils comme éclipse ou
    JDeveloper. Si l'application client est en Java, il est possible de créer
    un service proxy effectue les conversions SOAP / Java. Un simple cas de
    test JUnit peut être utilisé pour faire les essais de communication entre
    le client et le service web.</para>

    <para>Le bean client doit être déclaré dans un fichier Spring. La
    configuration du service client est composé d'un factory et d'un bean. Le
    factory est utilisé pour créer le bean de service. Le factory est
    configuré avec l'attribut "serviceClass" qui est le nom de classe de
    l'interface du service web ciblé. L'attribut "address" est l'adresse du
    service web. Le bean service est créé à l'aide du factory.</para>

    <para><example>
        <title>Fichier service.xml</title>

        <para><screen>&lt;bean id="serviceDossierFactory" class="org.apache.cxf.frontend.ClientProxyFactoryBean"&gt; 
  &lt;property name="serviceClass" value="com.nurun.preuveconcept.modele.dossier.service.ws.ServiceDossier"/&gt;
  &lt;property name="address" value="http://localhost:8080/web/ws/serviceDossier"/&gt;
&lt;/bean&gt;
&lt;bean id="<emphasis role="bold">serviceDossier</emphasis>" 
  class="com.nurun.preuveconcept.modele.dossier.service.ws.ServiceDossier" 
  factory-bean="serviceDossierFactory" 
  factory-method="create" /&gt;
</screen></para>
      </example></para>

    <para>Le service peut ensuite être testé à l'aide de la classe de cas de
    test JUnit. La classe de test doit instancier un contexte Spring puis
    récupérer le bean service déclaré plus haut. Le proxy qui est récupéré
    immite l'interface du service, on peut donc faire appel à toutes les
    méthodes comme s'il s'agissait du service lui-même.</para>

    <para><example>
        <title>Classe de test JUnit</title>

        <para><screen>package com.nurun.preuveconcept.client.test;

import java.util.List;

import junit.framework.TestCase;

import org.springframework.context.support.ClassPathXmlApplicationContext;

<emphasis role="bold">import com.nurun.preuveconcept.modele.dossier.service.ws.ServiceDossier;</emphasis>

public class ServiceDossierClientTest extends TestCase {

public ServiceDossierClientTest(String nom) {
  super(nom);
}

public void testTest() {
  ClassPathXmlApplicationContext contexte = new ClassPathXmlApplicationContext(new String[]{
    "com/nurun/preuveconcept/client/conf/contexte_client.xml"
  });
  <emphasis role="bold">ServiceDossier</emphasis> service = (ServiceDossier) contexte.getBean("<emphasis
              role="bold">serviceDossier</emphasis>");
  <emphasis role="bold">service.test()</emphasis>;
}
</screen></para>
      </example></para>
  </section>

  <section>
    <title>Sécurité des services web</title>

    <para></para>
  </section>

  <section>
    <title>Configuration Oracle JDeveloper et OC4J</title>

    <para>Il est nécessaire de faire quelques configurations spéciales pour
    être capable d'exécuter un services web CXF sur le serveur OC4J,
    JDeveloper et Oracle Application Server. Cette configuration spéciale est
    dûe au fait qu'une ancienne librairie JAX-WS est utilisée par Oracle et
    que celle-ci est chargée dans le classloader au démarrage de l'instance du
    serveur. Il faut modifier la configuration OC4J pour charger une version
    plus récente.</para>

    <para>JDeveloper 10.1.3</para>

    <para>Par le menu Tools / Embedded OC4J Server Preferences / Current
    Workspace / Library, ajouter les librairies suivantes :</para>

    <itemizedlist>
      <listitem>
        <para>xercesImpl.jar</para>
      </listitem>

      <listitem>
        <para>xml-apis.jar</para>
      </listitem>

      <listitem>
        <para>xalan.jar</para>
      </listitem>

      <listitem>
        <para>geronimo-ws-metadata_2.0_spec.jar</para>
      </listitem>
    </itemizedlist>

    <para>Trouver le fichier oc4j.jar utilisé par votre instance (jdeveloper,
    oc4j, ou oas). Modifier le fichier "boot.xml" situé dans le répertoire
    META-INF de l'archive.</para>

    <para><example>
        <title>Fichier boot.xml de l'archive oc4j.jar</title>

        <para><screen><emphasis role="bold">&lt;!--</emphasis> &lt;code-source 
       path="${oracle.home}/webservices/lib/jws-api.jar" 
       if="java.specification.version == /1\.[5-6]/"/&gt; <emphasis
              role="bold">--&gt;</emphasis></screen></para>
      </example>Dans le fichier oc4j-app.xml, ajouter les exclusions de
    librairies suivantes :</para>

    <para><example>
        <title>Fichier oc4j-app.xml</title>

        <para><screen>&lt;imported-shared-libraries&gt;
  &lt;remove-inherited name="oracle.toplink"/&gt;
  &lt;remove-inherited name="oracle.xml"/&gt;
&lt;/imported-shared-libraries&gt;
</screen></para>
      </example>Dans les préférences de votre projet web, aller dans le menu
    "Runner / Edit / Java Options" Ajouter la ligne suivante :</para>

    <para><example>
        <title>Java Options</title>

        <para><screen>-Xbootclasspath/p:"&lt;path to wsdlj&gt;\\wsdl4j-1.6.1.jar;&lt;path to jaxb2&gt;\\jaxb-api-2.0.jar"</screen></para>
      </example></para>
  </section>
</chapter>