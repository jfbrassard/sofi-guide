<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:exsl="http://exslt.org/common"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                exclude-result-prefixes="exsl"
                version='1.0'>

	<!-- It is important to use indent="no" here, otherwise verbatim -->
	<!-- environments get broken by indented tags...at least when the -->
	<!-- callout extension is used...at least with some processors -->
	<xsl:output method="xml" indent="no"/>
	
	<!-- La date de publication du guide -->
	<xsl:param name="publication.date">17 juin 2008</xsl:param>			
				
	<!-- La date de publication du guide -->
	<xsl:param name="publication.version">2.0.3</xsl:param>				

</xsl:stylesheet>