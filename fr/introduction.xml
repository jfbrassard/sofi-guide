<?xml version="1.0" encoding="UTF-8"?>
<chapter id="introduction" lang="fr">
  <title>Introduction</title>

  <section>
    <title>Qu'est-ce SOFI?</title>

    <para>Fondé en 2003, SOFI est un cadre d’applications Java et XML
    développé par Nurun inc. SOFI est née du fruit de l'expérience de nombreux
    développements Web en Java livrés avec succès en production depuis 2001.
    Vers la fin de 2004, SOFI est devenu le cadre d'applications de nombreuses
    organisations afin de développer leurs services en ligne (PES).
    Aujourd'hui, il y a plus de soixante systèmes qui sont développés sur la
    base du canevas SOFI, et avec l'aide du Portail SOFI, la communauté
    grandit de jour en jour. Contrairement à la plupart des autres cadres
    d'applications disponibles, SOFI se veut un cadre d'applications
    fonctionnel plutôt qu'un cadre technique.</para>

    <para><emphasis role="bold">SOFI = Cadre d'applications
    fonctionnels</emphasis><figure>
        <title>SOFI un cadre d'applications fonctionnelles pour vos solutions
        Web</title>

        <screenshot>
          <mediaobject>
            <imageobject>
              <imagedata align="center"
                         fileref="images/introduction/sofi-cadre-fonctionnelle.png" />
            </imageobject>
          </mediaobject>
        </screenshot>
      </figure></para>

    <itemizedlist>
      <listitem>
        <para>Un développement de système Web avec SOFI se base tout d’abord
        sur une infrastructure technologique. Des applications développées
        avec SOFI existent présentement en production sur des serveurs
        Windows, Linux et AIX. Cette possibilité assure, aux clients de SOFI,
        une indépendance du fournisseur technologique.</para>
      </listitem>

      <listitem>
        <para>Le développement est également basé sur la norme Java Enterprise
        Edition (JEE) de base, c’est-à-dire un serveur d’application tel que
        Oracle Application Server, BEA Weblogic, Websphere, JBoss et autres.
        Encore une fois, les applications SOFI sont indépendantes de la couche
        serveur utilisée pouvant ainsi être déployées sur plusieurs
        plates-formes différentes.</para>
      </listitem>

      <listitem>
        <para>Les applications SOFI s’appuient ensuite sur des cadres
        d’applications techniques matures et populaires. Il s’agit de
        logiciels libres considérés comme les meilleures pratiques du
        marché.</para>
      </listitem>

      <listitem>
        <para>La couche SOFI apparaît ensuite présentant tel que mentionné
        l’abstraction des cadres d’applications mais aussi une quantité
        importante de composants basés directement sur l’infrastructure Java
        EE (Enterprise Edition) qui sont uniques et exclusifs.</para>
      </listitem>

      <listitem>
        <para>Côté sécurité, SOFI offre la possibilité d’une solution
        d’authentification unique intégré soit avec un produit tel Microsoft
        Active Directory, Oracle SSO ou encore avec une application
        d’authentification unique maison développé par une
        organisation.</para>
      </listitem>

      <listitem>
        <para>Les solutions Web utilisent principalement la couche SOFI afin
        d’offrir leur services. C’est une solution clé en main, que ce soit
        pour développé la logique d’affaires ou encore les composants
        d’interfaces. Avec l'aide des composants SOFI, il est possible
        d'offrir des systèmes de grandes qualité car pratiquement tous les
        composants d'interfaces de SOFI respectent le <emphasis
        role="bold">Web 2.0</emphasis>.</para>
      </listitem>

      <listitem>
        <para>Notons finalement la présence dans toute l’architecture des
        patrons de conception, des bonnes pratiques du marché qui demeurent
        toujours au centre des considérations lors du développement avec SOFI,
        le but premier de SOFI est de ne pas réinventer la roue.</para>
      </listitem>
    </itemizedlist>

    <para><emphasis role="bold">SOFI = Support universel</emphasis></para>

    <para>Étant un cadre d'applications fonctionnnels, SOFI est basé sur
    plusieurs cadres d’applications provenant en grande majorité de la
    communauté des logiciels libres Java. Il bénéficie donc de l’expertise de
    milliers de programmeurs Java à la grandeur de la planète. De plus, SOFI
    est supporté par tous les serveurs d’applications J2EE, il est donc simple
    de déployer sur des plateformes tel que <emphasis role="bold">Oracle
    Applications Serveur</emphasis>, <emphasis role="bold">IBM
    WebSphere</emphasis>, <emphasis role="bold">BEA</emphasis>, <emphasis
    role="bold">Tomcat</emphasis> ou <emphasis
    role="bold">JBoss</emphasis>.</para>

    <figure>
      <title>SOFI :: une solution pour toutes les plateformes et outils de
      développement</title>

      <screenshot>
        <mediaobject>
          <imageobject>
            <imagedata align="center"
                       fileref="images/introduction/sofi-multiplateforme.png" />
          </imageobject>
        </mediaobject>
      </screenshot>
    </figure>

    <beginpage />

    <para><emphasis role="bold">L'évolution de SOFI</emphasis><figure>
        <title>Historique de l'évolution de SOFI</title>

        <screenshot>
          <mediaobject>
            <imageobject>
              <imagedata align="center"
                         fileref="images/introduction/sofi-historique-2.0.3.gif" />
            </imageobject>
          </mediaobject>
        </screenshot>
      </figure></para>

    <para>Voici les faits saiilants de l'évolution de SOFI :</para>

    <itemizedlist>
      <listitem>
        <para>2003 : Naissance du cadre sous le nom de code : <emphasis
        role="bold">Extension Framework</emphasis></para>
      </listitem>

      <listitem>
        <para>2004 : 1er partenaire et baptême officielle au nom de
        SOFI</para>
      </listitem>

      <listitem>
        <para>2005 : Évolution constante (sous forme de version de
        maintenance) avec de nombreux projets dans les organisations ayant
        choisi SOFI comme plateforme de développement</para>
      </listitem>

      <listitem>
        <para>2006 : Lancement de la communauté SOFI avec le site internet du
        <ulink url="http://sofi.nurunquebec.com">Portail SOFI</ulink>,
        Nomination au Octas sous la catégorie <emphasis role="bold">Réussite
        Commerciale</emphasis></para>
      </listitem>

      <listitem>
        <para>2007 : Lancement de <emphasis role="bold">SOFI 2.0</emphasis>,
        Ajout d'un forum de discussion sur le Portail SOFI</para>
      </listitem>

      <listitem>
        <para>2008 : Après de nombreuse migration d'applications de SOFI 1.x à
        SOFI 2.0, une importante version de maintenance est offert (version
        2.0.2), suivi d'une version 2.0.3 qui inclut beaucoup plus de
        documentation</para>
      </listitem>
    </itemizedlist>

    <para><emphasis role="bold">Les avantages de SOFI</emphasis></para>

    <para>SOFI inclut l'expertise de nombreux projets Web en Java et favorise
    l'ajout de nouvelles fonctionnalités via les équipes de développements
    Java de ces projets. Une prémisse forte importante à respecter lors d'un
    changement de SOFI, c'est que l'on doit conserver la rétrocompatibilité
    avec les versions antérieures. Ainsi, SOFI se voit être une solution très
    évolutive pour les systèmes en plus d'offrir un potentiel d’évolution hors
    pair. SOFI favorise aussi l'intégration de nouvelles technologies Java
    offertes sur le marché, ainsi toute communauté SOFI aura la chance de
    profiter de ces innovations ou améliorations dans leurs futurs
    développements. Voici une liste des avantages offerts à l'utilisation de
    SOFI :</para>

    <itemizedlist>
      <listitem>
        <para>Se doter d'un outil qui répond aux besoins utilisateurs</para>

        <itemizedlist>
          <listitem>
            <para>Déjà utilisé par des milliers d'utilisateurs</para>
          </listitem>

          <listitem>
            <para>Offre, clé en main, plusieurs fonctionnalités demandés par
            les utilisateurs</para>
          </listitem>
        </itemizedlist>
      </listitem>

      <listitem>
        <para>Offre une normalisation aux développeurs et aux
        utilisateurs</para>

        <itemizedlist>
          <listitem>
            <para>Des normes d'interfaces et de programmations sont
            offerts</para>
          </listitem>
        </itemizedlist>
      </listitem>

      <listitem>
        <para>Offre plusieurs solutions d'architecture (tant fonctionnel
        qu'organique)</para>

        <itemizedlist>
          <listitem>
            <para>Facilite ainsi le développement d’applications de grande
            envergure en Java</para>
          </listitem>

          <listitem>
            <para>Permets de développer des systèmes agiles et
            évolutifs</para>
          </listitem>

          <listitem>
            <para>Permets de profiter d'un maximum de réutilisation pour tout
            développement</para>
          </listitem>
        </itemizedlist>
      </listitem>

      <listitem>
        <para>Offre une infrastructure commune pour tous les services
        techniques normalement requis dans les développements d'applications
        Web</para>

        <itemizedlist>
          <listitem>
            <para>SOFI centralise l'utilisation de plusieurs services tel que
            :</para>

            <itemizedlist>
              <listitem>
                <para>messages</para>
              </listitem>

              <listitem>
                <para>libellés</para>
              </listitem>

              <listitem>
                <para>composant de navigation (menu, service, onglet,
                etc.)</para>
              </listitem>

              <listitem>
                <para>paramètre système et domaine de valeurs</para>
              </listitem>

              <listitem>
                <para>journalisation</para>
              </listitem>

              <listitem>
                <para>sécurité</para>
              </listitem>

              <listitem>
                <para>etc.</para>
              </listitem>
            </itemizedlist>
          </listitem>
        </itemizedlist>
      </listitem>

      <listitem>
        <para>Permets de profiter d'une communauté grandissante</para>

        <itemizedlist>
          <listitem>
            <para>Centralisation de l'information offert sur le Portail
            SOFI</para>
          </listitem>

          <listitem>
            <para>Forum de discussions</para>
          </listitem>

          <listitem>
            <para>Plusieurs experts et ressources d'expériences</para>
          </listitem>
        </itemizedlist>
      </listitem>

      <listitem>
        <para>Permets d'assurer aux programmeurs une utilisation des
        meilleures technologies offert sur la marché</para>

        <itemizedlist>
          <listitem>
            <para>Déjà basé sur plusieurs cadres d'applications renommés, SOFI
            offre la possiblité de grandir</para>
          </listitem>
        </itemizedlist>
      </listitem>

      <listitem>
        <para>Permets de réduire les coûts de développement d’une application
        en Java et cela peut importe son envergure</para>

        <itemizedlist>
          <listitem>
            <para>Permets de diminuer le temps de programmation d’unités de
            traitement</para>
          </listitem>

          <listitem>
            <para>Permets de réduire le temps ainsi que les coûts de
            maintenance de l’application</para>
          </listitem>
        </itemizedlist>
      </listitem>

      <listitem>
        <para>Permets d'ajouter des fonctionnalités d'une grande richesse avec
        des fonctionnalités Ajax intégrées</para>

        <itemizedlist>
          <listitem>
            <para>Se distingue par sa simplicité de développement et par les
            nombreux exemples d'utilisation</para>
          </listitem>
        </itemizedlist>
      </listitem>

      <listitem>
        <para>Permets de se conformer aux bonnes pratiques d’un développement
        par couches basées sur l’architecture orientée services (AOS)</para>

        <itemizedlist>
          <listitem>
            <para>SOFI favorise naturellement par son architecture le
            développement d'applications orientée-service</para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist>

    <beginpage />
  </section>

  <section>
    <title>SOFI et les autres cadres d'applications</title>

    <para>Basé sur le patron d'architecte MVC (Modèle-Vue-Contrôleur), SOFI
    est basé sur des cadres d'applications très reconnu dans le marché.</para>

    <figure>
      <title>SOFI et la simplification des cadres d'applications</title>

      <screenshot>
        <mediaobject>
          <imageobject>
            <imagedata align="center"
                       fileref="images/introduction/sofi-cadre-applications.png" />
          </imageobject>
        </mediaobject>
      </screenshot>
    </figure>

    <para><emphasis role="bold">Couche du modèle</emphasis></para>

    <para>Du côté du modèle, SOFI offre deux choix afin de développer de la
    logique d'affaires basé sur l'architecture orientée-service :<itemizedlist>
        <listitem>
          <para><emphasis role="bold">Oracle ADF</emphasis> : Cadre
          d'applications préconisé chez Oracle depuis la fin des années 1990.
          <itemizedlist>
              <listitem>
                <para>Très mature, ce cadre est très simple d'apprentissage
                des développeurs Java qui favorise l'utilisation du
                SQL.</para>
              </listitem>

              <listitem>
                <para>Présentement limité (via une utilisation gratuite) aux
                clients Oracle qui déploie sur un serveur d'applications
                Oracle</para>
              </listitem>

              <listitem>
                <para>Nombreux assistant graphique qui permettent de
                développer très rapidement,</para>
              </listitem>

              <listitem>
                <para>Il nécessite l'utilisation de Oracle JDeveloper.</para>
              </listitem>

              <listitem>
                <para>Site officiel : <ulink
                url="http://www.oracle.com/technology/products/adf/index.html">http://www.oracle.com/technology/products/adf/index.html</ulink></para>
              </listitem>
            </itemizedlist></para>
        </listitem>
      </itemizedlist><itemizedlist>
        <listitem>
          <para><emphasis role="bold">Spring-Hibernate</emphasis> : La
          combination du cadre Spring à celui d'Hibernate se veut un
          naturel.</para>

          <itemizedlist>
            <listitem>
              <para>Présentement la solution de développement de logique
              d'affaires la plus populaire dans le monde du logiciel libre
              (Open Source).</para>
            </listitem>

            <listitem>
              <para>Solution idéal lorsqu'on désire développer des
              applications basé sur un VRAI modèle de persistance objet
              (POJO).</para>
            </listitem>

            <listitem>
              <para>Site officiel : <ulink
              url="http://www.springframework.org">http://www.springframework.org</ulink>
              :: <ulink
              url="http://www.hibernate.org">http://www.hibernate.org</ulink></para>
            </listitem>
          </itemizedlist>
        </listitem>
      </itemizedlist></para>

    <para><emphasis role="bold">Couche de présentation</emphasis></para>

    <para>Du côté de la couche de présentation, SOFI est basé sur les
    technologies les plus matures et utilisés dans le monde Java.</para>

    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">Struts</emphasis> : Cadre d'applications
        Web le plus populaire et le plus mature en Java.</para>

        <itemizedlist>
          <listitem>
            <para>Basé sur le patron d'architecture MVC
            (Modèle-Vue-Contrôleur)</para>
          </listitem>

          <listitem>
            <para>Offre une architecture évolutive, flexible, léger offrant
            d'excellence performance</para>
          </listitem>

          <listitem>
            <para>Léger, flexible offrant d'excellente performance</para>
          </listitem>

          <listitem>
            <para>Utilisé dans la plupart des applications Web d'importance en
            Java</para>
          </listitem>

          <listitem>
            <para>Une grande masse de développeur ayant les connaissance de ce
            cadre d'applications</para>
          </listitem>

          <listitem>
            <para>L'utilisation de concept Ajax très simple a
            implémenter</para>
          </listitem>

          <listitem>
            <para>Site officiel : <ulink
            url="http://struts.apache.org">http://struts.apache.org</ulink></para>
          </listitem>
        </itemizedlist>
      </listitem>

      <listitem>
        <para><emphasis role="bold">Tiles</emphasis> : Cadre d'applications
        permettant le découpage des pages JSP</para>

        <itemizedlist>
          <listitem>
            <para>Offre une très grande modularité et un très peu
            couplage.</para>
          </listitem>

          <listitem>
            <para>Il est un assembleur de fragment de page, tel qu’un portail.
            Chaque fragment de page appelé « Tiles » peut être réutilisé tant
            de fois que vous désirez dans votre application.</para>
          </listitem>

          <listitem>
            <para>Tiles réduit considérablement de nombre d’inclusion
            (include) à faire dans vos pages JSP. comme conséquence,</para>

            <itemizedlist>
              <listitem>
                <para>Comme conséquence, le développement, la maintenance et
                le changement de « look » de l’application est beaucoup est de
                beaucoup simplifié.</para>
              </listitem>
            </itemizedlist>
          </listitem>

          <listitem>
            <para>Site officiel : <ulink
            url="http://tiles.apache.org">http://tiles.apache.org</ulink></para>
          </listitem>
        </itemizedlist>
      </listitem>

      <listitem>
        <para><emphasis role="bold">Commons</emphasis> : Projet d'Apache qui
        contient plusieurs librairies utilitaire commune.</para>

        <itemizedlist>
          <listitem>
            <para>SOFI utilise plusieurs de ces librairies pour la
            simplification de la couche de présentation.</para>

            <itemizedlist>
              <listitem>
                <para>Exemple : Le téléchargement de fichier d'un poste client
                vers le serveur.</para>
              </listitem>
            </itemizedlist>
          </listitem>

          <listitem>
            <para>Site officiel : <ulink
            url="http://commons.apache.org">http://commons.apache.org</ulink></para>
          </listitem>
        </itemizedlist>
      </listitem>

      <listitem>
        <para><emphasis role="bold">Velocity</emphasis> : Cadre d’applications
        qui permet de faire de la transformation de gabarit statique (par
        exemple HTML) et de le rendre dynamique avec l'aide du Java.</para>

        <itemizedlist>
          <listitem>
            <para>100 % conçu en Java, offre une bonne solution afin de
            encapsulé dans votre applications.</para>
          </listitem>

          <listitem>
            <para>SOFI utilise ce cadre afin de générer des composants
            d’interfaces tel que les menus, onglets, aide en ligne,
            etc.</para>
          </listitem>

          <listitem>
            <para>Site officiel : <ulink type=""
            url="http://velocity.apache.org">http://velocity.apache.org</ulink></para>
          </listitem>
        </itemizedlist>
      </listitem>

      <listitem>
        <para><emphasis role="bold">JSP</emphasis> : JSP est le standard Java
        afin de développer les interfaces Web dynamique.</para>

        <itemizedlist>
          <listitem>
            <para>SOFI offre plusieurs balises JSP avec fonctionnalités
            avancées.</para>
          </listitem>

          <listitem>
            <para>Les balises JSP offert par SOFI offre en grande majorité des
            fonctionnalités Ajax.</para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist>

    <para><emphasis role="bold">Couche d'intégration</emphasis></para>

    <para>Pour la couche d'intégration, SOFI utilise plusieurs cadres
    d'applications afin d'offrir de la journalisation, de la compilation, de
    la distribution, de la sérialisation XML, de la générations de rapports,
    etc. </para>

    <para>Voici une description des plus importants :</para>

    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">Spring</emphasis> : Cadre d'applications
        de référence pour la couche d'intégration. </para>

        <itemizedlist>
          <listitem>
            <para>SOFI favorise Spring dans la communication des applications
            avec l'infrastructure SOFI.</para>
          </listitem>

          <listitem>
            <para>Site officiel : <ulink
            url="http://www.springframework.org">http://www.springframework.org</ulink></para>
          </listitem>
        </itemizedlist>
      </listitem>

      <listitem>
        <para><emphasis role="bold">Log4j</emphasis> : Outil de journalisation
        des traitements Java</para>

        <itemizedlist>
          <listitem>
            <para>Simplicité de journalisation dans un fichier.</para>
          </listitem>

          <listitem>
            <para>Offre plusieus niveaux de journalisation (INFO, WARN,
            ERROR)</para>
          </listitem>

          <listitem>
            <para>Selon le niveau de journalisation configuré dans votre
            application, seulement ceux supérieure y seront affiché dans le
            fichier.</para>
          </listitem>

          <listitem>
            <para>SOFI utilise cet outil et favorise son utilisation.</para>
          </listitem>

          <listitem>
            <para>Site officiel : <ulink
            url="http://logging.apache.org/log4j/index.html">http://logging.apache.org/log4j/index.html</ulink></para>
          </listitem>
        </itemizedlist>
      </listitem>

      <listitem>
        <para><emphasis role="bold">Ivy</emphasis> : Outil de gestion des
        dépendances de librairies pour un projet</para>

        <itemizedlist>
          <listitem>
            <para>SOFI utilise cet outil afin d'offrir une simplicité
            d'installation selon la version désiré</para>
          </listitem>

          <listitem>
            <para>Site officiel : <ulink
            url="http://ant.apache.org/ivy/">http://ant.apache.org/ivy/</ulink></para>
          </listitem>
        </itemizedlist>
      </listitem>

      <listitem>
        <para><emphasis role="bold">XStream</emphasis> : Librarie spéicialisé
        dans la sérialisation et désérialisation des fichiers XML en objet
        Java et vice-versa.</para>

        <itemizedlist>
          <listitem>
            <para>Intégration de cette librairie afin d'offrir la possiblité
            de générer un document XML pour tous les objets de transferts
            développé avec SOFI.</para>
          </listitem>

          <listitem>
            <para>Possibilité de lire un fichier XML pour le convertir en
            objet de transfert. </para>
          </listitem>

          <listitem>
            <para>Site officiel : <ulink
            url="http://xstream.codehaus.org/">http://xstream.codehaus.org/</ulink></para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist>

    <beginpage />
  </section>

  <section>
    <title>SOFI et son infrastructure commune</title>

    <para>Développer des applications avec SOFI vous permet de réutiliser une
    infrastructure commune. L'infrastructure SOFI se veut des services commun
    organique nécessaire pour développer des systèmes transactionnels. Une
    même infrastructure permet de développer des sites grand public, des sites
    intranet ou encore des système de mission. Le tout est gérer par une
    console de gestion commune par environnement.</para>

    <figure>
      <title>L'utilisation de l'infrastructure SOFI</title>

      <screenshot>
        <mediaobject>
          <imageobject>
            <imagedata align="center"
                       fileref="images/sofi-infrastructure-commune.png" />
          </imageobject>
        </mediaobject>
      </screenshot>
    </figure>

    <para>L'infrastructure SOFI est offerte dans une version implémentée avec
    le canevas Oracle ADF Business Components (fortement utilisé avec l'outil
    JDeveloper) ou encore une autre implémentée par des canevas Open Source
    fort populaire soit Spring et Hibernate.</para>

    <para>Il est recommandé de lire le guide d'utilisation de la console de
    gestion de l'infrastructure SOFI disponible sur le portail SOFI ou encore
    le guide d'architecture de l'infrastructure SOFI pour en connaitre
    plus.</para>

    <itemizedlist>
      <listitem>
        <para>Afin de lire le guide d'utilisation de la console de gestion de
        l'infrastructure SOFI, accéder au Portail SOFI et suivre via le menu
        principal : <emphasis role="bold">Téléchargement -&gt; SOFI Version
        2.0 -&gt; Infrastructure</emphasis>.</para>
      </listitem>

      <listitem>
        <para>Afin de lire le document d'architecture organique de
        l'infrastructure SOFI, accéder au Portail SOFI et suivre le via le
        menu principal : <emphasis role="bold">Documentation -&gt;
        Architecture -&gt; Architecture orientée service
        SOFI</emphasis>.</para>
      </listitem>
    </itemizedlist>
  </section>
</chapter>